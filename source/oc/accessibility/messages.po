#. extracted from accessibility/inc
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2021-01-19 13:12+0100\n"
"PO-Revision-Date: 2023-02-01 09:33+0000\n"
"Last-Translator: Quentin PAGÈS <quentinantonin@free.fr>\n"
"Language-Team: Occitan <https://translations.documentfoundation.org/projects/libo_ui-master/accessibilitymessages/oc/>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Weblate 4.12.2\n"
"X-POOTLE-MTIME: 1539932026.000000\n"

#. be4e7
#: accessibility/inc/strings.hrc:24
msgctxt "RID_STR_ACC_NAME_BROWSEBUTTON"
msgid "Browse"
msgstr "Percórrer"

#. 42j6Y
#: accessibility/inc/strings.hrc:25
msgctxt "STR_SVT_ACC_ACTION_EXPAND"
msgid "Expand"
msgstr "Espandir"

#. 8MWFj
#: accessibility/inc/strings.hrc:26
msgctxt "STR_SVT_ACC_ACTION_COLLAPSE"
msgid "Collapse"
msgstr "Reduire"

#. zZTzc
#: accessibility/inc/strings.hrc:27
msgctxt "RID_STR_ACC_ACTION_CHECK"
msgid "Check"
msgstr "Marcar"

#. Kva49
#: accessibility/inc/strings.hrc:28
msgctxt "RID_STR_ACC_ACTION_UNCHECK"
msgid "Uncheck"
msgstr "Desmarcar"

#. nk4DD
#: accessibility/inc/strings.hrc:29
msgctxt "RID_STR_ACC_SCROLLBAR_NAME_VERTICAL"
msgid "Vertical scroll bar"
msgstr "Barra de defilament verticala"

#. FRA3z
#: accessibility/inc/strings.hrc:30
msgctxt "RID_STR_ACC_SCROLLBAR_NAME_HORIZONTAL"
msgid "Horizontal scroll bar"
msgstr "Barra de defilament orizontala"

#. DNmVr
#: accessibility/inc/strings.hrc:31
msgctxt "RID_STR_ACC_PANEL_DESCRIPTION"
msgid "Please press enter to go into child control for more operations"
msgstr "Quichatz sus Entrada per anar dins lo contraròtle enfant per d'operacions suplementàrias"

#. Fcjiv
#: accessibility/inc/strings.hrc:32
msgctxt "RID_STR_ACC_COLUMN_NUM"
msgid "Column %COLUMNNUMBER"
msgstr "Colomna %COLUMNNUMBER"

#. mAX2T
#: accessibility/inc/strings.hrc:33
msgctxt "RID_STR_ACC_ROW_NUM"
msgid "Row %ROWNUMBER"
msgstr "Linha %ROWNUMBER"

#. wH3TZ
msgctxt "stock"
msgid "_Add"
msgstr "_Apondre"

#. S9dsC
msgctxt "stock"
msgid "_Apply"
msgstr "_Aplicar"

#. TMo6G
msgctxt "stock"
msgid "_Cancel"
msgstr "_Anullar"

#. MRCkv
msgctxt "stock"
msgid "_Close"
msgstr "_Tampar"

#. nvx5t
msgctxt "stock"
msgid "_Delete"
msgstr "_Suprimir"

#. YspCj
msgctxt "stock"
msgid "_Edit"
msgstr "_Edicion"

#. imQxr
msgctxt "stock"
msgid "_Help"
msgstr "_Ajuda"

#. RbjyB
msgctxt "stock"
msgid "_New"
msgstr "_Novèl"

#. dx2yy
msgctxt "stock"
msgid "_No"
msgstr "_Non"

#. M9DsL
msgctxt "stock"
msgid "_OK"
msgstr "_D’acòrdi"

#. VtJS9
msgctxt "stock"
msgid "_Remove"
msgstr "_Levar"

#. C69Fy
msgctxt "stock"
msgid "_Reset"
msgstr "_Reïnicializar"

#. mgpxh
msgctxt "stock"
msgid "_Yes"
msgstr "_Òc"
